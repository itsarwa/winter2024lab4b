import java.util.Scanner;
public class VirtualPetApp {
	public static void main (String[] args) {
		Scanner scan = new Scanner(System.in);
		
		//Cat cat1 = new Cat();
		//cat1.name = "Peach";
		//cat1.color = "Orange";
		//cat1.age = 3;
		
		//System.out.println(cat1.presentCat());
		//cat1.catAge();
		
		//Part 2
		Cat[] clowder = new Cat[1];
		//System.out.println(clowder.length);
		for (int i = 0; i < clowder.length; i++) {
			
			
			System.out.println("What's the name of the cat?");
			String name=scan.nextLine();
			System.out.println("What's the color of the cat?");
			String color=scan.nextLine();
			System.out.println("How old is your cat?");
			int age=scan.nextInt();
			clowder[i] = new Cat(name, color, age);
		}
		scan.nextLine();
		
		System.out.println("What's the name of the last cat?");
		String newName = scan.nextLine();
		//clowder[clowder.length-1].setName(newName);
		
		//System.out.println("Info about your last cat: " + clowder[0].getName() + ", " + 
		//clowder[0].getColor() + ", " + clowder[0].getAge() + ".");
		//System.out.println(clowder[0].presentCat());
		//clowder[0].catAge();
		
		System.out.println("Info about your last cat (1): " + clowder[clowder.length -1].getName() + ", " + 
		clowder[clowder.length-1].getColor() + ", " + clowder[clowder.length-1].getAge() + ".");
		
		clowder[clowder.length-1].setName(newName);
		
		System.out.println("Info about your last cat (2): " + clowder[clowder.length -1].getName() + ", " + 
		clowder[clowder.length-1].getColor() + ", " + clowder[clowder.length-1].getAge() + ".");
		
	}
	
}